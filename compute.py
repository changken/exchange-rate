# -*- coding: utf-8 -*-
"""
Created on Thu Aug 29 20:57:36 2019

@author: p1011
"""

import numpy as np
from providers.amazon import AmazonRequests
from providers.rules import AmazonJPRule

def transformAmazonPrice(product_url, product_url_jp=None, *args, **kwargs): 
    assert isinstance(product_url, (list))
    assert isinstance(product_url_jp, (list)) or product_url_jp == None
    
    # 美國匯率
    rate = np.loadtxt('rate.txt')
    
    # 日本匯率
    rate_jp = np.loadtxt('rate_jp.txt')
    
    provider = AmazonRequests()
    
    product_url.extend(product_url_jp)
    
    for url in product_url:
        # 檢查網址是否為日本
        # 換算匯率
        if 'co.jp' in url:
            name, price = provider.retrieve(url, AmazonJPRule())
            
            price = price.split('￥')[1]
            #去除三位數撇節號
            price = filter(lambda ch: ch in '0123456789.', price)
            
            priceTransform = float(''.join(list(price))) * rate_jp
        else:
            name, price = provider.retrieve(url)
            priceTransform = float(price.split('$')[1]) * rate
            
        # 印出資訊
        print(name)
        print(priceTransform, "\n")

if __name__ == '__main__':
    # 美國商品
    product_url = [
            'https://www.amazon.com/Apple-MacBook-MJVM2LL-1-6GHz-Refurbished/dp/B01KIIQUFW/ref=lp_18332383011_1_1?currency=USD&ie=UTF8&qid=1567122030&sr=8-1&srs=18332383011',
            'https://www.amazon.com/Karma-Cards-Future-Through-Astrology/dp/0140154876/ref=sr_1_1?keywords=karma+cards&qid=1567123134&s=gateway&sr=8-1',
            'https://www.amazon.com/Karma-Cards-Monte-Farber/dp/B004HWSYGO/ref=sr_1_3?keywords=karma+cards&qid=1567123134&s=gateway&sr=8-3'
            'https://www.amazon.com/HP-8300-Elite-Computer-Quad-Core/dp/B01CV9G1BO/ref=lp_18332383011_1_6?srs=18332383011&ie=UTF8&qid=1567123779&sr=8-6',
            'https://www.amazon.com/Apple-Retina-Display-ME279LL-Refurbished/dp/B00TA9FCUU/ref=lp_18332383011_1_7?srs=18332383011&ie=UTF8&qid=1567123779&sr=8-7',
            'https://www.amazon.com/Dell-OptiPlex-Processor-Windows-Renewed/dp/B01AWOAUJY/ref=lp_18332383011_1_9?srs=18332383011&ie=UTF8&qid=1567123779&sr=8-9',
            'https://www.amazon.com/Karma-Cards-Fun-Use-Astrology/dp/1454926309/ref=tmm_other_meta_binding_swatch_0?_encoding=UTF8&qid=1567123134&sr=8-1'
            ]
    
    # 日本商品
    product_url_jp = [
          'https://www.amazon.co.jp/%E3%82%A8%E3%83%B3%E3%82%B8%E3%82%A7%E3%83%AB%E3%82%A2%E3%83%B3%E3%82%B5%E3%83%BC%E3%82%AA%E3%83%A9%E3%82%AF%E3%83%AB%E3%82%AB%E3%83%BC%E3%83%89-%E3%82%AA%E3%83%A9%E3%82%AF%E3%83%AB%E3%82%AB%E3%83%BC%E3%83%89%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA-%E3%83%89%E3%83%AA%E3%83%BC%E3%83%B3%E3%83%BB%E3%83%90%E3%83%BC%E3%83%81%E3%83%A5%E3%83%BC/dp/4904665848/ref=sr_1_1?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&crid=10LQ44CG6FKA5&keywords=angel+answers+oracle+cards&qid=1567125102&s=electronics&sprefix=angel+answer%2Celectronics%2C336&sr=8-1',
          'https://www.amazon.co.jp/%E3%83%AD%E3%83%9E%E3%83%B3%E3%82%B9%E3%82%A8%E3%83%B3%E3%82%B8%E3%82%A7%E3%83%AB%E3%82%AA%E3%83%A9%E3%82%AF%E3%83%AB%E3%82%AB%E3%83%BC%E3%83%89-%E6%97%A5%E6%9C%AC%E8%AA%9E%E7%89%88%E8%AA%AC%E6%98%8E%E6%9B%B8%E4%BB%98-%E3%82%AA%E3%83%A9%E3%82%AF%E3%83%AB%E3%82%AB%E3%83%BC%E3%83%89%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA-%E3%83%89%E3%83%AA%E3%83%BC%E3%83%B3-%E3%83%BB%E3%83%90%E3%83%BC%E3%83%81%E3%83%A5%E3%83%BC/dp/4904665503/ref=pd_bxgy_14_img_3/355-6582283-9122336?_encoding=UTF8&pd_rd_i=4904665503&pd_rd_r=48851b76-d94b-493c-a254-12272e0f1ea3&pd_rd_w=Psrst&pd_rd_wg=YVpko&pf_rd_p=e1d64852-a4db-4f50-95af-f6bb1415e9c1&pf_rd_r=W5CKYJ7XBG6JQJFYA7YN&psc=1&refRID=W5CKYJ7XBG6JQJFYA7YN',
          'https://www.amazon.co.jp/TWIN-FLAME-ANGEL-ORACLE-Book/dp/1085980138/ref=sr_1_7?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&crid=10LQ44CG6FKA5&keywords=angel+answers+oracle+cards&qid=1567125102&s=electronics&sprefix=angel+answer%2Celectronics%2C336&sr=8-7',
          'https://www.amazon.co.jp/Karma-Cards-Fun-Use-Astrology/dp/1454926309/ref=sr_1_fkmr1_1?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&keywords=karma+cards+oracle+cards&qid=1567125213&s=electronics&sr=8-1-fkmr1',
          'https://www.amazon.co.jp/Work-Your-Light-Oracle-Cards/dp/178180995X/ref=sr_1_fkmr2_2?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&keywords=karma+cards+oracle+cards&qid=1567125213&s=electronics&sr=8-2-fkmr2'
          ]

    # 轉換
    transformAmazonPrice(product_url, product_url_jp)