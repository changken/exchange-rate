# -*- coding: utf-8 -*-
"""
Main File
"""

import numpy as np
from selenium import webdriver
from providers.visa import Visa

provider = Visa(driver=webdriver.Firefox())

bs, rate = provider.exchange()

print("1 %s 可轉換為 %f %s"%(provider.fromCurr, rate, provider.toCurr))

Rate = np.array([rate]);

np.savetxt('rate.txt', Rate)

wait = ''
while True:
    print('關閉請鍵入 \'q\' !')
    wait = input()
    if wait == 'q':
        print('bye bye~')
        bs.close()
        break