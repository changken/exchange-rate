# -*- coding: utf-8 -*-
"""
Requests + pyquery
"""

from providers.visa import VisaRequests
import numpy as np

def convertCurrency(area='USD'):
    provider = VisaRequests(toCurr=area)
    
    r, rate, date = provider.exchange()
    
    Rate = np.array([float(rate)])
    
    # 儲存匯率到txt檔
    if area == 'USD':
        np.savetxt('rate.txt', Rate)
    elif area == 'JPY':
        np.savetxt('rate_jp.txt', Rate)
    
    return rate,  date
