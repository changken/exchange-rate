# -*- coding: utf-8 -*-
"""
Amazon 
"""

import requests
from pyquery import PyQuery as pq
from providers.rules import AmazonUSRule

class AmazonRequests:
    def __init__(self):
        pass

    def retrieve(self, url, rule = AmazonUSRule()):
        headers={
            'user-agent':'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        }
        
        r = requests.get(url, headers=headers)
        
        if r.status_code == requests.codes.ok:
            q = pq(r.text)
            
            # 擷取規則
            name, price = rule.rule(q)
            
            return name, price
        else:
            r.raise_for_status()