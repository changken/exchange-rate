# -*- coding: utf-8 -*-
"""
Amazon Rule
"""

# 美國的名稱及價格擷取規則
class AmazonUSRule:
    def __init__(self):
        pass
    
    def rule(self, q):
        name = q('#productTitle').text()
                 
        # way 1
        price = q('#priceblock_ourprice').text()
            
        # way 2
        if price == '':
            price = q('div.inlineBlock-display span.a-size-medium.a-color-price.offer-price.a-text-normal').text()
            
        # way 3
        if price == '':
            price = q('div.a-text-center.a-spacing-mini span.a-color-price').text()
        
        return name, price
    
# 日本的名稱及價格擷取規則
class AmazonJPRule:
    def __init__(self):
        pass
    
    def rule(self, q):
        name = q('#productTitle').text()
                 
        # way 1
        price = q('#priceblock_ourprice').text()
            
        # way 2
        if price == '':
            price = q('span.inlineBlock-display span.a-size-medium.a-color-price.offer-price.a-text-normal').text()
            
        # way 3
        #if price == '':
        #    price = q('div.a-text-center.a-spacing-mini span.a-color-price').text()
        
        return name, price