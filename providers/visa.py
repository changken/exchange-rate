# -*- coding: utf-8 -*-
"""
Visa Exchange class
"""

import requests
from pyquery import PyQuery as pq
import time

class VisaRequests:
    def __init__(self, fromCurr = 'TWD', toCurr = 'USD', fee = '1.5', exchangedate = None):     
        self.fromCurr = fromCurr
        self.toCurr = toCurr
        self.fee = fee
        self.exchangedate = exchangedate
    
    def exchange(self):
        data = {
                'fromCurr': self.fromCurr,
                'toCurr': self.toCurr,
                'fee': self.fee,
                #'exchangedate': self.exchangedate #'08/29/2019'
        }
		
        # 如果沒有指定日期就以今天的日期為主
        if self.exchangedate == None:
            data['exchangedate'] = time.strftime("%m/%d/%Y", time.localtime())
        
        headers = {
                'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
        }
        
        r = requests.get('https://www.visa.com.tw/travel-with-visa/exchange-rate-calculator.html', params=data, headers=headers)
        
        # 使用PyQuery來解析回應!
        d = pq(r.text)
        
        # 找到匯率
        rate = d('td.exchangeRateText p:eq(1) span strong:eq(1)').text()
        
        # 找到時間
        date = d('td.exchangeRateText div p:eq(1)').text()
        
        return r, rate, date