# -*- coding: utf-8 -*-
"""
Created on Thu Aug 29 22:25:13 2019

@author: p1011
"""

from selenium import webdriver

#引入select
from selenium.webdriver.support.ui import Select 

class Visa:
    def __init__(self, fromCurr = 'TWD', toCurr = 'USD', fee = '1.5', driver = None):
        if driver == None:
            self.driver = webdriver.Chrome()
        else:
            self.driver = driver
            
        self.fromCurr = fromCurr
        self.toCurr = toCurr
        self.fee = fee
    
    def exchange(self):
        #啟動firefox!
        bs = self.driver
        
        #到這個網址
        bs.get('https://www.visa.com.tw/travel-with-visa/exchange-rate-calculator.html')
        
        #選擇台幣
        fromCurr = Select(bs.find_element_by_id('fromCurr'))
        fromCurr.select_by_value(self.fromCurr)
        
        #選擇美金
        toCurr = Select(bs.find_element_by_id('toCurr'))
        toCurr.select_by_value(self.toCurr)
        
        #填入手續費
        fee = bs.find_element_by_id('fee')
        fee.send_keys(self.fee)
        
        #按下計算匯率
        submitBtn = bs.find_element_by_id('submitButton')
        submitBtn.click()
        
        #多少新台幣
        exchangeRate = bs.find_element_by_xpath('//td[@class="exchangeRateText"]/p[2]/span/strong[2]')
        exchangeRateNumber = float(exchangeRate.text)
        
        return bs, exchangeRateNumber