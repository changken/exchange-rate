# -*- coding: utf-8 -*-
'''
for exchange2.py test case
only valid in 2019.9.4!!!!!
'''

from exchange2 import convertCurrency

rate_usd, date = convertCurrency() 
assert rate_usd == '31.858820'

print(rate_usd)

rate_jpy, date = convertCurrency(area='JPY')
assert rate_jpy == '0.301321'

print(rate_jpy)
