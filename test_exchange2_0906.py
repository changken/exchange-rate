# -*- coding: utf-8 -*-
'''
for exchange2.py test case
only valid in 2019.9.6!!!!!
'''

from exchange2 import convertCurrency

rate_usd, date = convertCurrency() 
assert rate_usd == '31.728900'

print(rate_usd)

rate_jpy, date = convertCurrency(area='JPY')
assert rate_jpy == '0.298539'

print(rate_jpy)
